package com.example.androiddictionary;

public class dictionary {
	

	//private variables
	int Col_id;
	String Wordname;
	String Meaning;
	private String Antonym;
	
	// Empty constructor
	public dictionary(){
		
	}
	
	// constructor
		public dictionary(int id, String name, String Meaning,String Antonym){
			this.Col_id = id;
			this.Wordname = name;
			this.Meaning = Meaning;
			this.Antonym = Antonym;
			
		}
		
		// constructor
		public dictionary(String name, String Meaning,String Antonym){
			this.Wordname = name;
			this.Meaning = Meaning;
			this.Antonym = Antonym;
		}
		// getting ID
		public int getID(){
			return this.Col_id;
		}
		
		// setting id
		public void setID(int id){
			this.Col_id = id;
		}
		
		// getting name
		public String getWord(){
			return this.Wordname;
		}
		
		// setting name
		public void setWord(String name){
			this.Wordname = name;
		}
		
		// getting word Meaning
		public String getMeaning(){
			return this.Meaning;
		}
		
		// setting meaning
		public void setMeaning(String Meaning){
			this.Meaning = Meaning;
		}

		public String getAntonym() {
			return this.Antonym;
		}
	
		public void setAntonym( String Antonym){
			this.Antonym = Antonym;
		}

}
