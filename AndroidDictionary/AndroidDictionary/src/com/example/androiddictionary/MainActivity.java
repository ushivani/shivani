package com.example.androiddictionary;

import com.example.androiddictionary.GetMeaningActivity;
import com.example.androiddictionary.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

import android.widget.EditText;

public class MainActivity extends Activity {
	
	 public final static String EXTRA_MESSAGE = "com.example.Dictionary.MESSAGE";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		 DatabaseHandler db = new DatabaseHandler(this);
		 
		 db.addWord(new dictionary("transient", "impermanent,temporary","immanent"));
		 db.addWord(new dictionary("absent", "lacking,missing", "present"));
		 db.addWord(new dictionary("discordant", "at variance,discrepant", "accordant"));
		 db.addWord(new dictionary("encouragement", "boost", "discouragement"));
		 db.addWord(new dictionary("occupant", "resident", "nonresident"));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	 /** Called when the user clicks the Send button */
    public void SearchWord(View view) {
        // Do something in response to button
    	Intent intent = new Intent(this, GetMeaningActivity.class);
    	EditText editText = (EditText) findViewById(R.id.edit_message);
    	
    	 
    	
    	String message = editText.getText().toString();
    	intent.putExtra(EXTRA_MESSAGE, message);
    	startActivity(intent);
    }

}
