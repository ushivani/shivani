package com.example.androiddictionary;




import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {
	
	// All Static variables
		// Database Version
		private static final int DATABASE_VERSION = 1;

		// Database Name
		private static final String DATABASE_NAME = "DictionaryDB";

		// Dictionary table name
		private static final String TABLE = "DictTable";

		// Dictionary  Table Columns names
		private static final String KEY_Id = "Col_id";
		private static final String KEY_Word = "Wordname";
		private static final String KEY_Meaning = "Meaning";
		private static final String KEY_Antonym = "Antonym";

		public DatabaseHandler(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			String Create_Dict_Table = "CREATE TABLE " + TABLE + "("
					+ KEY_Id + " INTEGER PRIMARY KEY," + KEY_Word + " TEXT,"
					+ KEY_Meaning + " TEXT" + "," + KEY_Antonym + " TEXT " + ")";
			db.execSQL(Create_Dict_Table);
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Drop older table if existed
			db.execSQL("DROP TABLE IF EXISTS " + TABLE);

			// Create tables again
			onCreate(db);
			
		}
		
		
		// Adding new word
		public void addWord(dictionary word) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_Word, word.getWord()); 
			values.put(KEY_Meaning, word.getMeaning());
			values.put(KEY_Antonym, word.getAntonym());

			// Inserting Row
			db.insert(TABLE, null, values);
			db.close(); // Closing database connection
		}
		
		// Getting Word Meaning
		 public dictionary getMeaning(String message1) {
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.query(TABLE, new String[] { KEY_Id,
					KEY_Word, KEY_Meaning ,KEY_Antonym}, KEY_Word + "=?",
					new String[] { String.valueOf(message1) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			dictionary Meaning = new dictionary(Integer.parseInt(cursor.getString(0)),
					cursor.getString(1), cursor.getString(2),cursor.getString(3));
			// return contact
			return Meaning;
		}

		 // Deleting single row
		    public void deleteWord(dictionary word) {
		        SQLiteDatabase db = this.getWritableDatabase();
		        db.delete(TABLE, KEY_Id + " = ?",
		                new String[] { String.valueOf(word.getID()) });
		        db.close();
		    }
		

}
