package com.example.androiddictionary;





import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class GetMeaningActivity extends MainActivity {
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.get_meaning);
	        
	 
	 // Get the message from the intent
	    Intent intent = getIntent();
	    String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

	    // Create the text view
	   // TextView textView = new TextView(this);
	   // textView.setTextSize(40);
	  //  textView.setText(message);

	    // Set the text view as the activity layout
	   // setContentView(textView);
	    
	    
	    getMeaning1(message);
	    //dictionary meaning = getMeaning(message);
	
	 }

	public void getMeaning1(String message1) {
		
		///getMeaning(message1);
		DatabaseHandler myDatabaseHelper = new DatabaseHandler(GetMeaningActivity.this);
		 dictionary text= myDatabaseHelper.getMeaning(message1);
		
		 TextView textView = new TextView(this);
		 textView.setText((CharSequence) text);
	}

}
